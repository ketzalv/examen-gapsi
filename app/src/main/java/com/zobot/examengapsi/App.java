package com.zobot.examengapsi;

import android.app.Application;

import com.zobot.examengapsi.data.local.Preferences;

/**
 * Created by alba_ on 22/12/2017.
 */

public class App extends Application {

    private static App m_singleton;
    private Preferences prefs = null;

    @Override
    public void onCreate() {
        super.onCreate();
        m_singleton = this;
        prefs = new Preferences(this);
    }

    public Preferences getPrefs() {
        return prefs;
    }
    public static App getInstance() {
        return m_singleton;
    }
}
