package com.zobot.examengapsi.data.local;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zobot.examengapsi.data.model.Record;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.zobot.examengapsi.data.local.PreferencesContract.LIST_LAST_SEARCHED;
import static com.zobot.examengapsi.utils.UtilsPreferences.objectToString;
import static com.zobot.examengapsi.utils.UtilsPreferences.stringToObject;

/**
 * Created by alba_ on 22/12/2017.
 */

public class Preferences {

    private SharedPreferences preferences;

    public Preferences(Context context) {
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void saveData(String key, String data) {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putString(key, data);
        editor.commit();
    }

    public void saveData(String key, Serializable data) {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putString(key, objectToString(data));
        editor.commit();
    }

    public void saveDataBool(String key, boolean data) {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putBoolean(key, data);
        editor.commit();
    }

    public boolean containsData(String key) {
        return this.preferences.contains(key);
    }

    public Serializable loadData(String key, Boolean isObject) {
        return stringToObject(this.preferences.getString(key, null));
    }
    public boolean loadBoolean(String key) {
        return preferences.getBoolean(key, false);
    }

    public <T extends Serializable> T loadData(String key, Class<T> type) {
        return type.cast(stringToObject(this.preferences.getString(key, null)));
    }

    public String loadString(String key) {
        return preferences.getString(key, "");
    }

    public List<Record> loadLastSearched(){
       String loadSearched = loadString(LIST_LAST_SEARCHED);
        Type founderListType = new TypeToken<ArrayList<Record>>(){}.getType();

        List<Record> founderList = new Gson().fromJson(loadSearched, founderListType);

        return  founderList != null ? founderList : new ArrayList<Record>();

    }

    public void clearPreferences() {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.clear();
        editor.commit();
        return;
    }

    public void clearPreference(String key) {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.remove(key);
        editor.commit();
        return;
    }

    public void saveDataInt(String key, int data) {
        SharedPreferences.Editor editor = this.preferences.edit();
        String stringData = Integer.toString(data);
        editor.putString(key, stringData);
        editor.commit();
    }

}