package com.zobot.examengapsi.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alba_ on 22/12/2017.
 */

public class Attributes {

    private String[] isShipPleasant;

    @SerializedName("product.brand")
    private String[] productBrand;

    @SerializedName("product.smallImage")
    private String[] productSmallImage;

    private String[] limitedPiecesEndDate;

    private String[] materialGroup;
    private String[] isExclusiveProduct;
    @SerializedName("product.displayName")
    private String[] productDisplayName;
    private String[] presaleEndDate;
    private String[] isGiftWithPurchase;
    private String[] productType;
    private String[] availabilityShopStartDate;
    @SerializedName("sku.galleriaImage")
    private String[] skuGalleriaImage;
    @SerializedName("sku.list_Price")
    private String[] skuList_Price;
    private String[] expressDeliveryStartDate;
    private String[] importBook;
    private String[] exclusivePromotionStartDate;
    private String[] isExclusivePrice;
    private String[] isAvailabilityShop;
    private String[] groupType;
    private String[] expressDeliveryEndDate;

    public Attributes() {
    }

    public String[] getIsShipPleasant() {
        return isShipPleasant;
    }

    public void setIsShipPleasant(String[] isShipPleasant) {
        this.isShipPleasant = isShipPleasant;
    }

    public String[] getProductBrand() {
        return productBrand;
    }

    public void setProductBrand(String[] productBrand) {
        this.productBrand = productBrand;
    }

    public String[] getProductSmallImage() {
        return productSmallImage;
    }

    public void setProductSmallImage(String[] productSmallImage) {
        this.productSmallImage = productSmallImage;
    }

    public String[] getLimitedPiecesEndDate() {
        return limitedPiecesEndDate;
    }

    public void setLimitedPiecesEndDate(String[] limitedPiecesEndDate) {
        this.limitedPiecesEndDate = limitedPiecesEndDate;
    }

    public String[] getMaterialGroup() {
        return materialGroup;
    }

    public void setMaterialGroup(String[] materialGroup) {
        this.materialGroup = materialGroup;
    }

    public String[] getIsExclusiveProduct() {
        return isExclusiveProduct;
    }

    public void setIsExclusiveProduct(String[] isExclusiveProduct) {
        this.isExclusiveProduct = isExclusiveProduct;
    }

    public String[] getProductDisplayName() {
        return productDisplayName;
    }

    public void setProductDisplayName(String[] productDisplayName) {
        this.productDisplayName = productDisplayName;
    }

    public String[] getPresaleEndDate() {
        return presaleEndDate;
    }

    public void setPresaleEndDate(String[] presaleEndDate) {
        this.presaleEndDate = presaleEndDate;
    }

    public String[] getIsGiftWithPurchase() {
        return isGiftWithPurchase;
    }

    public void setIsGiftWithPurchase(String[] isGiftWithPurchase) {
        this.isGiftWithPurchase = isGiftWithPurchase;
    }

    public String[] getProductType() {
        return productType;
    }

    public void setProductType(String[] productType) {
        this.productType = productType;
    }

    public String[] getAvailabilityShopStartDate() {
        return availabilityShopStartDate;
    }

    public void setAvailabilityShopStartDate(String[] availabilityShopStartDate) {
        this.availabilityShopStartDate = availabilityShopStartDate;
    }

    public String[] getSkuGalleriaImage() {
        return skuGalleriaImage;
    }

    public void setSkuGalleriaImage(String[] skuGalleriaImage) {
        this.skuGalleriaImage = skuGalleriaImage;
    }

    public String[] getSkuList_Price() {
        return skuList_Price;
    }

    public void setSkuList_Price(String[] skuList_Price) {
        this.skuList_Price = skuList_Price;
    }

    public String[] getExpressDeliveryStartDate() {
        return expressDeliveryStartDate;
    }

    public void setExpressDeliveryStartDate(String[] expressDeliveryStartDate) {
        this.expressDeliveryStartDate = expressDeliveryStartDate;
    }

    public String[] getImportBook() {
        return importBook;
    }

    public void setImportBook(String[] importBook) {
        this.importBook = importBook;
    }

    public String[] getExclusivePromotionStartDate() {
        return exclusivePromotionStartDate;
    }

    public void setExclusivePromotionStartDate(String[] exclusivePromotionStartDate) {
        this.exclusivePromotionStartDate = exclusivePromotionStartDate;
    }

    public String[] getIsExclusivePrice() {
        return isExclusivePrice;
    }

    public void setIsExclusivePrice(String[] isExclusivePrice) {
        this.isExclusivePrice = isExclusivePrice;
    }

    public String[] getIsAvailabilityShop() {
        return isAvailabilityShop;
    }

    public void setIsAvailabilityShop(String[] isAvailabilityShop) {
        this.isAvailabilityShop = isAvailabilityShop;
    }

    public String[] getGroupType() {
        return groupType;
    }

    public void setGroupType(String[] groupType) {
        this.groupType = groupType;
    }

    public String[] getExpressDeliveryEndDate() {
        return expressDeliveryEndDate;
    }

    public void setExpressDeliveryEndDate(String[] expressDeliveryEndDate) {
        this.expressDeliveryEndDate = expressDeliveryEndDate;
    }

    /*
        "exclusiveProductStartDate": [
        "1510063808"
                                                ],
        "limitedPiecesStartDate": [
        "1510063808"
                                                ],
        "productAvgRating": [
        "4.4"
                                                ],
        "exclusivePriceEndDate": [
        "1510063808"
                                                ],
        "isExpressDelivery": [
        "false"
                                                ],
        "sku.repositoryId": [
        "1063763108"
                                                ],
        "sku.thumbnailImage": [
        "https://ss423.liverpool.com.mx/xl/1063763108.jpg"
                                                ],
        "giftWithPurchaseStartDate": [
        "1510063808"
                                                ],
        "minimumPromoPrice": [
        "0.0"
                                                ],
        "sku.promoPrice": [
        "0.0"
                                                ],
        "exclusivePriceStartDate": [
        "1510063808"
                                                ],
        "product.repositoryId": [
        "1063763108"
                                                ],
        "product.largeImage": [
        "https://ss423.liverpool.com.mx/lg/1063763108.jpg"
                                                ],
        "isNewProduct": [
        "false"
                                                ],
        "product.productRatingCount": [
        "39"
                                                ],
        "isLimitedPieces": [
        "false"
                                                ],
        "isExclusivDiscount": [
        "false"
                                                ],
        "isExclusivePackage": [
        "false"
                                                ],
        "maximumPromoPrice": [
        "0.0"
                                                ],
        "newProductStartDate": [
        "1510063808"
                                                ],
        "latestPartsStartDate": [
        "1510063808"
                                                ],
        "isPresale": [
        "false"
                                                ],
        "exclusivDiscountEndDate": [
        "1510063808"
                                                ],
        "shipPleasantEndDate": [
        "1510063808"
                                                ],
        "sku.largeImage": [
        "https://ss423.liverpool.com.mx/lg/1063763108.jpg"
                                                ],
        "exclusivePackageEndDate": [
        "1510063808"
                                                ],
        "exclusivePackageStartDate": [
        "1510063808"
                                                ],
        "isLatestParts": [
        "false"
                                                ],
        "deliveryOnlyStoreStartDate": [
        "1510063808"
                                                ],
        "availabilityShopEndDate": [
        "1510063808"
                                                ],
        "exclusivDiscountStartDate": [
        "1510063808"
                                                ],
        "sku.smallImage": [
        "https://ss423.liverpool.com.mx/sm/1063763108.jpg"
                                                ],
        "deliveryOnlyStoreEndDate": [
        "1510063808"
                                                ],
        "giftWithPurchaseEndDate": [
        "1510063808"
                                                ],
        "presaleStartDate": [
        "1510063808"
                                                ],
        "exclusivePromotionEndDate": [
        "1510063808"
                                                ],
        "newProductEndDate": [
        "1510063808"
                                                ],
        "noRefund": [
        "true"
                                                ],
        "InventoryStatus": [
        "1000"
                                                ],
        "latestPartsEndDate": [
        "1510063808"
                                                ],
        "minimumListPrice": [
        "12299.0"
                                                ],
        "shipPleasantStartDate": [
        "1510063808"
                                                ],
        "maximumListPrice": [
        "12299.0"
                                                ],
        "sortPrice": [
        "12299.000000"
                                                ],
        "allAncestors.repositoryId": [
        "cat5100256",
                "cat5400005",
                "cat5420008",
                "cat5420045",
                "cat5420049",
                "cat5760059",
                "cat670055",
                "cat940612",
                "catst1577755",
                "catst1578010",
                "catst1578012",
                "catst4445114",
                "catst4445115",
                "catst4445130",
                "catst4445131"
                                                ],
        "skipInventory": [
        "dontSkip"
                                                ],
        "isExclusivePromotion": [
        "false"
                                                ],
        "isDeliveryOnlyStore": [
        "false"
                                                ],
        "exclusiveProductEndDate": [
        "1510063808"
                                                ],
        "Language": [
        "español"
                                                ],
        "sku.sale_Price": [
        "12299.0"
                                                ]
                                                */
}
