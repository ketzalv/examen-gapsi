package com.zobot.examengapsi.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by alba_ on 22/12/2017.
 */

public class Content extends Model {

    private String title;
//    private String headerContent = "";
    private String[] originalTerms;
    private String metaKeywords;
    private String metaDescription;
    private String ruleLimit;
    private String contentCollection;
    private List<Content> contents;
    private List<Content> mainContent;
    private List<Content> secondaryContent;
    @SerializedName("endeca:contentPath")
    private String contentPath;
    @SerializedName("atg:currentSiteProductionURL")
    private String currentSiteProductionURL;
    @SerializedName("endeca:siteRootPat")
    private String siteRootPat;
    private List<SortOptionLabel> sortOptions;
    private int firstRecNum;
    private int lastRecNum;
    private int recsPerPage;
    private List<Record> records;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String[] getOriginalTerms() {
        return originalTerms;
    }

    public void setOriginalTerms(String[] originalTerms) {
        this.originalTerms = originalTerms;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getRuleLimit() {
        return ruleLimit;
    }

    public void setRuleLimit(String ruleLimit) {
        this.ruleLimit = ruleLimit;
    }

    public String getContentCollection() {
        return contentCollection;
    }

    public void setContentCollection(String contentCollection) {
        this.contentCollection = contentCollection;
    }

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }

    public List<Content> getMainContent() {
        return mainContent;
    }

    public void setMainContent(List<Content> mainContent) {
        this.mainContent = mainContent;
    }

    public List<Content> getSecondaryContent() {
        return secondaryContent;
    }

    public void setSecondaryContent(List<Content> secondaryContent) {
        this.secondaryContent = secondaryContent;
    }

    public String getContentPath() {
        return contentPath;
    }

    public void setContentPath(String contentPath) {
        this.contentPath = contentPath;
    }

    public String getCurrentSiteProductionURL() {
        return currentSiteProductionURL;
    }

    public void setCurrentSiteProductionURL(String currentSiteProductionURL) {
        this.currentSiteProductionURL = currentSiteProductionURL;
    }

    public String getSiteRootPat() {
        return siteRootPat;
    }

    public void setSiteRootPat(String siteRootPat) {
        this.siteRootPat = siteRootPat;
    }

    public List<SortOptionLabel> getSortOptions() {
        return sortOptions;
    }

    public void setSortOptions(List<SortOptionLabel> sortOptions) {
        this.sortOptions = sortOptions;
    }

    public int getFirstRecNum() {
        return firstRecNum;
    }

    public void setFirstRecNum(int firstRecNum) {
        this.firstRecNum = firstRecNum;
    }

    public int getLastRecNum() {
        return lastRecNum;
    }

    public void setLastRecNum(int lastRecNum) {
        this.lastRecNum = lastRecNum;
    }

    public int getRecsPerPage() {
        return recsPerPage;
    }

    public void setRecsPerPage(int recsPerPage) {
        this.recsPerPage = recsPerPage;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }
}
