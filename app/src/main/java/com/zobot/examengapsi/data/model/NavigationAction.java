package com.zobot.examengapsi.data.model;

/**
 * Created by alba_ on 22/12/2017.
 */

public class NavigationAction {
    private String navigationState; //: "?No=%7Boffset%7D&Nrpp=%7BrecordsPerPage%7D&Ntt=xbox&d3106047a194921c01969dfdec083925=json&s=xbox",
    private String contentPath; //: "/listing",
    private String siteRootPath; //: "/pages",
    private String label; //: null
}
