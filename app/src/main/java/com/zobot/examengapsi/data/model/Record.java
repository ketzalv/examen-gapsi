package com.zobot.examengapsi.data.model;

import java.util.List;

/**
 * Created by alba_ on 22/12/2017.
 */

public class Record extends Model {
    private RecordAction detailsAction;
    private int numRecords;
    private Attributes attributes;
    private List<Record> records;

    public Record() {
    }
    public static Record toTest(){
        Record record = new Record();
        Attributes attrs = new Attributes();
        attrs.setProductDisplayName(new String[]{"Prueba 00"});
        attrs.setSkuList_Price(new String[]{"Prueba 00"});
        record.setAttributes(attrs);
        return record;
    }
    public RecordAction getDetailsAction() {
        return detailsAction;
    }

    public void setDetailsAction(RecordAction detailsAction) {
        this.detailsAction = detailsAction;
    }

    public int getNumRecords() {
        return numRecords;
    }

    public void setNumRecords(int numRecords) {
        this.numRecords = numRecords;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }
}
