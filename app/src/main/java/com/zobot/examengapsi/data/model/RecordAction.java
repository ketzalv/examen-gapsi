package com.zobot.examengapsi.data.model;

import com.zobot.examengapsi.data.model.Model;

/**
 * Created by alba_ on 22/12/2017.
 */

class RecordAction extends Model {

    private String recordState; //": "/consola-xbox-one-x-1-tb/1063763108?d3106047a194921c01969dfdec083925=json&s=xbox",
    private String contentPath; //": "/listing",
    private String siteRootPath; //": "/pages",
    private String label; //": null

    public RecordAction() {
    }

    public String getRecordState() {
        return recordState;
    }

    public void setRecordState(String recordState) {
        this.recordState = recordState;
    }

    public String getContentPath() {
        return contentPath;
    }

    public void setContentPath(String contentPath) {
        this.contentPath = contentPath;
    }

    public String getSiteRootPath() {
        return siteRootPath;
    }

    public void setSiteRootPath(String siteRootPath) {
        this.siteRootPath = siteRootPath;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
