package com.zobot.examengapsi.data.remote;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.zobot.examengapsi.data.model.ResponseQuery;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by alba_ on 22/12/2017.
 */

public class AsyncRequest extends AsyncTask<Void, Void, String> {

    private String mRequest = "";
    private IAsyncResponse mResponse = null;

    public AsyncRequest(String request, IAsyncResponse response){
        this.mRequest = request;
        this.mResponse = response;
    }

    @Override
    protected String doInBackground(Void... voids) {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://www.liverpool.com.mx/tienda/?s="+ mRequest +"&d3106047a194921c01969dfdec083925=json")
                .get()
                .addHeader("cache-control", "no-cache")
                .addHeader("postman-token", "1dbd8383-9616-4458-c645-c89dc3fb040b")
                .build();

        try {
            Response response = client.newCall(request).execute();
            Log.d(getClass().getSimpleName(), "Response > " + response.toString());
//                Log.d(getClass().getSimpleName(), "body > " + response.body().string());
            ResponseBody responseBodyCopy = response.peekBody(Long.MAX_VALUE);

            Log.d(getClass().getSimpleName(), "Response body >" + new Gson().fromJson(response.body().string(), ResponseQuery.class));
            return responseBodyCopy.string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(mResponse != null){
            if(s != null){
                mResponse.onSuccess(new Gson().fromJson(s, ResponseQuery.class));
            }else{
                mResponse.onFailed("Ocurrio un error al hacer la consulta");
            }
        }
    }
}
