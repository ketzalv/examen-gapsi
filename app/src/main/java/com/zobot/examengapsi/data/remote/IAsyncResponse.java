package com.zobot.examengapsi.data.remote;

import com.zobot.examengapsi.data.model.ResponseQuery;

/**
 * Created by alba_ on 22/12/2017.
 */

public interface IAsyncResponse {
    void onSuccess(ResponseQuery query);
    void onFailed(String message);
}
