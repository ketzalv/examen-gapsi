package com.zobot.examengapsi.ui._base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.zobot.examengapsi.R;

import static com.zobot.examengapsi.utils.Constants.SHOW_MESSAGE;
import static com.zobot.examengapsi.utils.UtilsUi.showSnackBar;
import static com.zobot.examengapsi.utils.UtilsUi.showToast;

/**
 * Created by alba_ on 22/12/2017.
 */

public abstract class AbstractActivity extends AppCompatActivity implements IEvent, View.OnClickListener {

    private FragmentControllerDelegate mFCDelegate = null;

    @IdRes
    protected abstract int setIdContainerFragments();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFCDelegate = new FragmentControllerDelegate(this, setIdContainerFragments());
    }
    protected void loadFragment(@NonNull AbstractFragment fragment) {
        mFCDelegate.loadFragment(fragment, setIdContainerFragments(), FragmentControllerDelegate.TRANSITION.NONE, false);
    }

    protected void loadFragment(@NonNull AbstractFragment fragment, @NonNull FragmentControllerDelegate.TRANSITION direction) {
        mFCDelegate.loadFragment(fragment, setIdContainerFragments(), direction, false);
    }

    protected AbstractFragment getCurrentFragment(){
        return mFCDelegate.getCurrentFragment();
    }
    protected AbstractFragment getCurrentFragment(@IdRes int idContainer){
        return mFCDelegate.getCurrentFragment(idContainer);
    }
    @Override
    public void onEvent(int event, Object data) {
        switch (event){
            case SHOW_MESSAGE:
                if (data != null && data instanceof String) {
                    showMessage((String) data);
                }
                break;
        }
    }
    public void showMessage(String message) {
        View rootCoordinator = findViewById(R.id.lyt_main_coordinator);
        if (rootCoordinator != null) {
            showSnackBar(rootCoordinator, message);
        } else {
            showToast(message, this);
        }
    }
    public IEvent getIEvent(){
        return this;
    }

    @Override
    public void onClick(View view) {

    }

    protected void hideSoftKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
