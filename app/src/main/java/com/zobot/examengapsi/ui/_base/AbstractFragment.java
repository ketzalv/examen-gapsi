package com.zobot.examengapsi.ui._base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.zobot.examengapsi.App;
import com.zobot.examengapsi.data.local.Preferences;
import com.zobot.examengapsi.data.remote.AsyncRequest;
import com.zobot.examengapsi.data.remote.IAsyncResponse;

import static com.zobot.examengapsi.utils.Constants.SHOW_MESSAGE;
import static com.zobot.examengapsi.utils.UtilsUi.showSnackBar;

/**
 * Created by alba_ on 22/12/2017.
 */

public abstract class AbstractFragment extends Fragment implements View.OnClickListener, IAsyncResponse {
    protected String mFragmentTag = "";
    protected IEvent mEvent = null;
    private Preferences mPrefs = null;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof IEvent){
            this.mEvent = (IEvent) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = App.getInstance().getPrefs();
        mFragmentTag = setFragmentTag() != null ? setFragmentTag() : getClass().getSimpleName();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
    @Override
    public void onDetach() {
        super.onDetach();
        this.mEvent = null;
    }

    protected void sendEvent(int event, Object data){
        if(mEvent != null){
            mEvent.onEvent(event, data);
        }
    }

    protected abstract String setFragmentTag();

    public String getFragmentTag() {
        return mFragmentTag;
    }

    @Override
    public void onClick(View view) {

    }

    public IEvent getEvent() {
        return mEvent;
    }

    public void setEvent(IEvent mEvent) {
        this.mEvent = mEvent;
    }

    public Preferences getPrefs() {
        return mPrefs;
    }
    protected void showMessage(String message){
        sendEvent(SHOW_MESSAGE, message);
    }
    public void sendRequest(String query){
        new AsyncRequest(query, this).execute();
    }

}

