package com.zobot.examengapsi.ui._base;

import android.support.annotation.AnimRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.zobot.examengapsi.R;

/**
 * @author jvazquez
 *
 * Created by jvazquez on 11/08/2017.
 *
 * This Delegate handle fragments in activities
 *
 */

public class FragmentControllerDelegate {
    @IdRes
    protected int idContainerFragments = -1;
    protected FragmentTransaction mFragmentTransaction = null;
    protected FragmentManager mFragmentManager = null;

    private AbstractActivity mActivity = null;

    public FragmentControllerDelegate(AbstractActivity activity){
        this.mActivity = activity;
        mFragmentManager = mActivity.getSupportFragmentManager();
    }
    public FragmentControllerDelegate(AbstractActivity activity, int idContainerFragments){
        this.mActivity = activity;
        mFragmentManager = mActivity.getSupportFragmentManager();
        this.idContainerFragments = idContainerFragments;
    }

    public int getIdContainerFragments() {
        return idContainerFragments;
    }

    public void setIdContainerFragments(int idContainerFragments) {
        this.idContainerFragments = idContainerFragments;
    }

    public enum TRANSITION {
        FORDWARD(R.anim.slide_from_right, R.anim.slide_to_left),
        BACK(R.anim.slide_from_left, R.anim.slide_to_right),
        FADE(android.R.anim.fade_in, android.R.anim.fade_out),
        NONE(0, 0);

        private int enterAnimation;
        private int exitAnimation;

        private TRANSITION(@AnimRes int enterAnimation, @AnimRes int exitAnimation) {
            this.enterAnimation = enterAnimation;
            this.exitAnimation = exitAnimation;
        }

        public int getEnterAnimation() {
            return enterAnimation;
        }

        public int getExitAnimation() {
            return exitAnimation;
        }
    }
    public void loadFragment(@NonNull AbstractFragment fragment, @IdRes int idContainer, @NonNull TRANSITION direction,
                             boolean addToBackStack) {
        this.idContainerFragments = idContainer;
        mFragmentTransaction = mFragmentManager.beginTransaction();
        if (!direction.equals(TRANSITION.NONE)) {
            mFragmentTransaction.setCustomAnimations(direction.getEnterAnimation(), direction.getExitAnimation());
        }
        mFragmentTransaction.replace(idContainer, fragment, fragment.getFragmentTag());
        if (addToBackStack) {
            mFragmentTransaction.addToBackStack(fragment.getFragmentTag());
        }
        mFragmentTransaction.commit();
    }
    public AbstractFragment getCurrentFragment() {
        if (idContainerFragments != -1) {
            return getCurrentFragment(idContainerFragments);
        }
        return null;
    }
    public AbstractFragment getCurrentFragment(@IdRes int idContainer) {
        return (AbstractFragment) mFragmentManager.findFragmentById(idContainer);
    }
}
