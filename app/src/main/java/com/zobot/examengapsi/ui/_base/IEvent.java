package com.zobot.examengapsi.ui._base;

/**
 * Created by alba_ on 22/12/2017.
 */

public interface IEvent {
    void onEvent(int id, Object data);
}
