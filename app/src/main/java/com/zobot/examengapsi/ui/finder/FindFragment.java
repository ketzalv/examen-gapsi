package com.zobot.examengapsi.ui.finder;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.zobot.examengapsi.R;
import com.zobot.examengapsi.data.model.Content;
import com.zobot.examengapsi.data.model.Record;
import com.zobot.examengapsi.data.model.ResponseQuery;
import com.zobot.examengapsi.ui._base.AbstractFragment;

import java.util.ArrayList;
import java.util.List;

import static com.zobot.examengapsi.data.local.PreferencesContract.LIST_LAST_SEARCHED;
import static com.zobot.examengapsi.utils.Constants.FRAGMENT_FIND;
import static com.zobot.examengapsi.utils.UtilsUi.showToast;

/**
 * Created by alba_ on 22/12/2017.
 */

public class FindFragment extends AbstractFragment {
    private FindRvAdapter mAdapter = null;
    private List<Record> mList = new ArrayList<>();
    private View progressView = null;

    public static FindFragment newInstance() {

        Bundle args = new Bundle();

        FindFragment fragment = new FindFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mList = getPrefs().loadLastSearched();
        this.mAdapter = new FindRvAdapter(getActivity(), getEvent(), mList);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_find, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initRecycler((RecyclerView) view.findViewById(R.id.rv_find_fragment));
    }

    private void initViews(View view) {
        if(view != null){
            final EditText edtSearch = view.findViewById(R.id.edt_search);
            edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                        if(!edtSearch.getText().toString().isEmpty()){
                            sendRequest(edtSearch.getText().toString());
                            showProgress(true);
                        }else{
                            onFailed("Ingresa un criterio de busqueda");
                        }
                    }
                    return false;
                }
            });
            progressView = view.findViewById(R.id.progress);
            showProgress(false);
        }
    }

    private void initRecycler(RecyclerView recyclerView) {
        if(recyclerView != null){
            GridLayoutManager lm = new GridLayoutManager(getContext(), 2);
            lm.setOrientation(GridLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(lm);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(mAdapter);
        }
    }
    @Override
    protected String setFragmentTag() {
        return FRAGMENT_FIND;
    }

    @Override
    public void onSuccess(ResponseQuery query) {
        showProgress(false);
        if(query != null && query.getContents() != null && !query.getContents().isEmpty()){
            List<Content> contents = query.getContents().get(0).getMainContent();
            for(Content content: contents){
                if(content.getType().equalsIgnoreCase("ContentSlotMain")){
                    if(content.getContents() != null && !content.getContents().isEmpty()){
                        Content currentContent = content.getContents().get(0);
                        if(currentContent.getType().equalsIgnoreCase("ResultsList") && currentContent.getRecords() != null && !currentContent.getRecords().isEmpty()) {
                            mList.clear();
                            mList.addAll(currentContent.getRecords());
                            getPrefs().saveData(LIST_LAST_SEARCHED, currentContent.getRecords().toString());
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }
        }else{
            onFailed("No se encontraron resultados!");
        }
    }

    @Override
    public void onFailed(String message) {
        showProgress(false);
        showMessage(message);
    }
    private void showProgress(boolean show){
        if(progressView != null){
            progressView.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
        }
    }
}
