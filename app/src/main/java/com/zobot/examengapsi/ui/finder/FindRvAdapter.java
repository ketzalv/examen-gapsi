package com.zobot.examengapsi.ui.finder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zobot.examengapsi.R;
import com.zobot.examengapsi.data.model.Record;
import com.zobot.examengapsi.ui._base.IEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alba_ on 22/12/2017.
 */

public class FindRvAdapter extends RecyclerView.Adapter<FindRvAdapter.ViewHolder> {
    private List<Record> mList = new ArrayList<>();
    protected IEvent mEvent = null;

    private Context mContext = null;

    public FindRvAdapter(Context context, IEvent event, List<Record> list){
        this.mList = list;
        this.mEvent = event;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_find_product, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Record currentRecord = getElementAtPosition(position);
        if(currentRecord.getAttributes() != null)
            if(currentRecord.getAttributes().getProductSmallImage() != null && currentRecord.getAttributes().getProductSmallImage().length > 0){
                Picasso.with(getContext())
                        .load(currentRecord.getAttributes().getProductSmallImage()[0])
                        .into(holder.mIvThumbnails);
            }
            if(currentRecord.getAttributes().getProductDisplayName() != null && currentRecord.getAttributes().getProductDisplayName().length > 0){
                holder.mTvName.setText(currentRecord.getAttributes().getProductDisplayName()[0]);

            }
            String price = (currentRecord.getAttributes().getSkuList_Price() != null && currentRecord.getAttributes().getSkuList_Price().length > 0) ?
                    currentRecord.getAttributes().getSkuList_Price()[0] : null;
            String isAvailabilityShop = (currentRecord.getAttributes().getIsAvailabilityShop() != null && currentRecord.getAttributes().getIsAvailabilityShop().length > 0) ?
                currentRecord.getAttributes().getIsAvailabilityShop()[0] : null;
            if(price != null && isAvailabilityShop != null){
                holder.mTvDescription.setText(getContext().getString(R.string.msg_description_product, price, isAvailabilityShop));
            }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
    public boolean isEmpty(){
        return mList != null && mList.isEmpty();
    }

    protected void sendEvent(int event, Object data){
        if(mEvent != null){
            mEvent.onEvent(event, data);
        }
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }
    protected Record getElementAtPosition(int position){
        return mList.get(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mIvThumbnails            = null;
        TextView mTvName        = null;
        TextView mTvDescription = null;

        public ViewHolder(View itemView) {
            super(itemView);
            mIvThumbnails = itemView.findViewById(R.id.image_product_thumbnails);
            mTvName = itemView.findViewById(R.id.text_name);
            mTvDescription = itemView.findViewById(R.id.text_description);
        }
    }
}
