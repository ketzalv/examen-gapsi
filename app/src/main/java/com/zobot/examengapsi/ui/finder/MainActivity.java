package com.zobot.examengapsi.ui.finder;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.zobot.examengapsi.R;
import com.zobot.examengapsi.ui._base.AbstractActivity;

import static com.zobot.examengapsi.utils.UtilsUi.showSnackBar;
import static com.zobot.examengapsi.utils.UtilsUi.showToast;

public class MainActivity extends AbstractActivity {

    @Override
    protected int setIdContainerFragments() {
        return R.id.lyt_main_fragment_container;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        setTitle("");
//        findViewById(R.id.btnExecute).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new AsyncRequest("xbox").execute();
//            }
//        });
        loadFragment(FindFragment.newInstance());

    }
}
