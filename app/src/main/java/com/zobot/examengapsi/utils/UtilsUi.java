package com.zobot.examengapsi.utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

/**
 * Created by alba_ on 22/12/2017.
 */

public class UtilsUi {
    public static void showToast(String message, Context context){
        Toast.makeText(context,message, Toast.LENGTH_SHORT).show();
    }
    public static void showSnackBar(View coordinatorLayout, String message, String actionName, View.OnClickListener listener){
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG).setAction(actionName, listener).show();
    }
    public static void showSnackBar(View coordinatorLayout, String message){
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }
    public static void showSnackBar(View coordinatorLayout, String message, String actionName, int duration, View.OnClickListener listener){
        Snackbar.make(coordinatorLayout, message, duration).setAction(actionName, listener).show();
    }
}